ARG PYTHON_VERSION
ARG POETRY_VERSION

FROM python:${PYTHON_VERSION}-bullseye
COPY ["docker-poetry.sh", "/usr/local/bin/"]

RUN ["docker-poetry.sh"]
CMD ["poetry"]
